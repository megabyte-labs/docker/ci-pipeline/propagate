# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.8](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/propagate/compare/v0.0.7...v0.0.8) (2021-06-16)

### [0.0.7](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/propagate/compare/v0.0.6...v0.0.7) (2021-06-14)

### [0.0.6](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/propagate/compare/v0.0.5...v0.0.6) (2021-06-13)

### [0.0.5](https://gitlab.com/ProfessorManhattan/code/compare/v0.0.3...v0.0.5) (2021-05-20)

### [0.0.4](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/propagate/compare/v0.0.3...v0.0.4) (2021-05-20)

### [0.0.3](https://gitlab.com/ProfessorManhattan/code/compare/v0.0.2...v0.0.3) (2021-05-13)

### 0.0.2 (2021-05-13)
